package ru.tsc.chertkova.tm;

import ru.tsc.chertkova.tm.constant.ArgumentConst;
import ru.tsc.chertkova.tm.constant.TerminalConst;
import ru.tsc.chertkova.tm.model.Command;
import ru.tsc.chertkova.tm.util.NumberUtil;

import java.util.Scanner;

public final class Application {

    public static void main(final String[] args) {
        displayWelcome();
        run(args);
        process();
    }

    private static void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            System.out.println("ENTER COMMAND:");
            command = scanner.nextLine();
            run(command);
            System.out.println();
        }
    }

    private static void displayHelp() {
        System.out.println("[HELP]");
        System.out.println(Command.VERSION);
        System.out.println(Command.ABOUT);
        System.out.println(Command.HELP);
        System.out.println(Command.INFO);
        System.out.println(Command.EXIT);
    }

    private static void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.7.0");
    }

    private static void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Yuliya Chertkova");
        System.out.println("ychertkova@t1-consulting.com");
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void exit() {
        System.exit(0);
    }

    private static void run(final String[] args) {
        if (args == null || args.length < 1) return;
        final String param = args[0];
        if (param == null || param.isEmpty()) return;
        switch (param) {
            case ArgumentConst.CMD_HELP:
                displayHelp();
                break;
            case ArgumentConst.CMD_VERSION:
                displayVersion();
                break;
            case ArgumentConst.CMD_ABOUT:
                displayAbout();
                break;
            case ArgumentConst.CMD_INFO:
                showInfo();
                break;
            default:
                showError(param);
        }
    }

    private static void run(final String param) {
        if (param == null || param.isEmpty()) return;
        switch (param) {
            case TerminalConst.CMD_HELP:
                displayHelp();
                break;
            case TerminalConst.CMD_VERSION:
                displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                displayAbout();
                break;
            case TerminalConst.INFO:
                showInfo();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            default:
                showError(param);
        }
    }

    private static void showError(final String param) {
        System.out.printf("Error! This argument '%s' not supported...\n", param);
    }

    private static void showInfo() {
        System.out.println("[INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

}
