package ru.tsc.chertkova.tm.model;

import ru.tsc.chertkova.tm.constant.ArgumentConst;
import ru.tsc.chertkova.tm.constant.TerminalConst;

public class Command {
    public static Command ABOUT = new Command(
            TerminalConst.CMD_ABOUT, ArgumentConst.CMD_ABOUT,
            "Display developer info."
    );

    public static Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Terminate close application."
    );

    public static Command HELP = new Command(
            TerminalConst.CMD_HELP, ArgumentConst.CMD_HELP,
            "Display list of terminal commands."
    );

    public static Command VERSION = new Command(
            TerminalConst.CMD_VERSION, ArgumentConst.CMD_VERSION,
            "Display program version."
    );

    public static Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.CMD_INFO,
            "Show system info."
    );

    private String name = "";

    private String argument = "";

    private String description = "";

    public Command() {
    }

    public Command(String name) {
        this.name = name;
    }

    public Command(final String name, final String argument, final String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}
