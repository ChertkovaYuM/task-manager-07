package ru.tsc.chertkova.tm.constant;

public final class TerminalConst {

    public static final String CMD_HELP = "help";

    public static final String CMD_VERSION = "version";

    public static final String CMD_ABOUT = "about";

    public static final String EXIT = "exit";

    public static final String INFO = "info";

}
