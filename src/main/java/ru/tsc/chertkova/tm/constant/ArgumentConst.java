package ru.tsc.chertkova.tm.constant;

public class ArgumentConst {

    public static final String CMD_HELP = "-h";

    public static final String CMD_VERSION = "-v";

    public static final String CMD_ABOUT = "-a";

    public static final String CMD_INFO = "-i";

}
